import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Screens/CartScreen.dart';
import '../Screens/HomeScreen.dart';
import '../Screens/Profile.dart';
import '../Screens/WishListScreen.dart';

class CustomBottomNaveBar extends StatelessWidget {
  const CustomBottomNaveBar({
    super.key,
    required this.media,
  });

  final Size media;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: media.width * 0.9,
      height: media.height * 0.08,
      decoration: BoxDecoration(
          color: Colors.deepOrange, borderRadius: BorderRadius.circular(15)),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        IconButton(
          icon: const Icon(
            size: 30,
            Icons.home,
            color: Colors.white,
          ),
          onPressed: () {
            Get.toNamed(MyHomePage.routeName);
          },
        ),
        IconButton(
          icon: const Icon(
            size: 30,
            Icons.shopping_bag_outlined,
            color: Colors.white,
          ),
          onPressed: () {
            Get.toNamed(CartScreen.routeName);
          },
        ),
        IconButton(
          icon: const Icon(
            size: 30,
            Icons.person,
            color: Colors.white,
          ),
          onPressed: () {
            Get.toNamed(ProfileScreen.routeName);
          },
        )
      ]),
    );
  }
}
