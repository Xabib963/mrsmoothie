import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/model/Catigory.dart';
import 'package:shopsy/model/Product.dart';

import '../Screens/CatalogScreen.dart';

class CarouselCard extends StatelessWidget {
  final Catigory? cate;
  final Product? product;

  const CarouselCard({super.key, this.cate, this.product});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        product == null
            ? Get.toNamed(
                CatalogScreen.routeName,
                arguments: cate,
              )
            : {};
      },
      child: Container(
        margin: const EdgeInsets.all(5.0),
        child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            child: Stack(
              children: <Widget>[
                product == null
                    ? Image.asset(
                        cate!.imgUrl,
                        fit: BoxFit.fill,
                        width: 1000,
                      )
                    : Image.network(
                        product!.imgUrl,
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) => Container(
                            width: 100,
                            height: 100,
                            alignment: Alignment.center,
                            child: const Icon(Icons.error_outline,
                                color: Colors.red)),
                        width: 1000,
                      ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  right: 0.0,
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.deepOrange.withOpacity(0.7),
                          Colors.deepOrange.withOpacity(0.1)
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      ),
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    child: Text(
                      product == null ? cate!.name : product!.name,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
