import 'package:flutter/material.dart';

class Textintro extends StatelessWidget {
  const Textintro({
    super.key,
    required this.str,
  });
  final String str;

  @override
  Widget build(BuildContext context) {
    return Text(
      "$str INFORMATION",
      style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 25),
    );
  }
}
