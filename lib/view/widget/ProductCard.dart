// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/addtowishlistController.dart';

import 'package:shopsy/model/Cartpromodel.dart';
import 'package:shopsy/model/Product.dart';

import '../../controller/Cartcontroller.dart';

import '../Screens/Productscreen.dart';

class ProductCard extends StatelessWidget {
  ProductCard.catalog({
    super.key,
    required this.media,
    required this.iswishlist,
    required this.product,
    required this.width,
    required this.height,
    this.addtoWishController,
  });
  ProductCard.prduct({
    super.key,
    required this.media,
    required this.iswishlist,
    required this.product,
    required this.width,
    required this.height,
    this.addtoWishController,
  });
  ProductCard.wishlist({
    super.key,
    required this.media,
    required this.iswishlist,
    required this.product,
    required this.width,
    required this.height,
    required this.addtoWishController,
  });
  final Product product;
  final Size media;
  final double width;
  final double height;
  final bool iswishlist;
  final AddToWishlistController? addtoWishController;

  CartController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(ProductScreen.routeName, arguments: product),
      child: Container(
        clipBehavior: Clip.hardEdge,
        constraints: BoxConstraints(
          maxHeight: height,
          maxWidth: width,
          minWidth: width,
          minHeight: height,
        ),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
                fit: StackFit.loose,
                clipBehavior: Clip.hardEdge,
                children: [
                  Image.network(product.imgUrl,
                      fit: BoxFit.fill,
                      errorBuilder: (context, error, stackTrace) => Container(
                          width: 100,
                          height: 100,
                          alignment: Alignment.center,
                          child: const Icon(Icons.error_outline,
                              color: Colors.red)),
                      height: constraints.maxHeight,
                      width: double.infinity),
                  Positioned(
                      bottom: constraints.maxHeight * 0.000,
                      left: 0,
                      child: Container(
                        width: iswishlist
                            ? constraints.maxWidth * 0.80
                            : constraints.maxWidth * 0.8,
                        height: iswishlist
                            ? constraints.maxHeight * 0.35
                            : constraints.maxHeight * 0.45,
                        color: Colors.deepOrangeAccent.withOpacity(0.3),
                        child: Container(
                          margin: const EdgeInsets.all(5),
                          color: Colors.deepOrange[700]!.withOpacity(0.7),
                          child: Row(children: [
                            Expanded(
                              flex: 3,
                              child: Text(
                                product.name,
                                maxLines: 2,
                                overflow: TextOverflow.clip,
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineLarge!
                                    .copyWith(fontSize: 21),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Expanded(
                                flex: 1,
                                child: IconButton(
                                    onPressed: () {
                                      controller.addtoCart(Cartproduct(
                                          product.name,
                                          product.imgUrl,
                                          product.price));
                                      Get.snackbar("Added Succesfuly", 'done',
                                          snackStyle: SnackStyle.FLOATING,
                                          messageText: Text(
                                            "safsaffd",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyLarge!
                                                .copyWith(color: Colors.black),
                                          ),
                                          titleText: Text(
                                            "Added Succesfuly",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyLarge!
                                                .copyWith(color: Colors.green),
                                          ),
                                          snackPosition: SnackPosition.BOTTOM);
                                    },
                                    icon: Icon(
                                        size: iswishlist
                                            ? constraints.maxWidth * 0.08
                                            : constraints.maxWidth * 0.12,
                                        color: Colors.white,
                                        Icons.add_circle))),
                            iswishlist
                                ? Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        onPressed: () {
                                          addtoWishController!
                                              .removeFromWishlist(
                                            product,
                                          );
                                          Get.snackbar(
                                              "Added Succesfuly", 'done',
                                              snackStyle: SnackStyle.FLOATING,
                                              messageText: Text(
                                                "safsaffd",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyLarge!
                                                    .copyWith(
                                                        color: Colors.green),
                                              ),
                                              titleText: Text(
                                                "Removed Succefully",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyLarge!
                                                    .copyWith(
                                                        color: Colors.black),
                                              ),
                                              snackPosition:
                                                  SnackPosition.BOTTOM);
                                        },
                                        icon: Icon(
                                            size: constraints.maxWidth * 0.08,
                                            color: Colors.white,
                                            Icons.delete)))
                                : const SizedBox()
                          ]),
                        ),
                      ))
                ]);
          },
        ),
      ),
    );
  }
}
