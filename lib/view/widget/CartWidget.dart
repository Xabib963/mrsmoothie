import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';
import 'package:shopsy/model/Cartpromodel.dart';

import '../../main.dart';

class Cartwid extends StatelessWidget {
  final Cartproduct prod;

  Cartwid({super.key, required this.prod});
  CartController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      width: medias(context).width,
      child: Row(children: [
        Container(
          width: medias(context).width * 0.3,
          clipBehavior: Clip.hardEdge,
          padding: EdgeInsets.zero,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
          child: Image.network(
            fit: BoxFit.cover,
            errorBuilder: (context, error, stackTrace) => Container(
                width: 100,
                height: 100,
                alignment: Alignment.center,
                child: const Icon(Icons.error_outline, color: Colors.red)),
            prod.imgUrl,
          ),
        ),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                prod.name,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge!
                    .copyWith(fontSize: 20),
              ),
              Text("${prod.price}\$",
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(fontSize: 18))
            ],
          ),
        )),
        Expanded(
            child: Row(
          children: [
            IconButton(
                onPressed: () {
                  controller.add(prod);
                },
                icon: const Icon(Icons.add_circle_outlined,
                    color: Colors.deepOrange)),
            GetBuilder<CartController>(
                init: controller,
                builder: (controller) => Text("${prod.quantity}")),
            IconButton(
                onPressed: () {
                  controller.remove(prod);
                },
                icon: const Icon(
                  Icons.remove_circle,
                  color: Colors.deepOrange,
                ))
          ],
        ))
      ]),
    );
  }
}
