import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/AuthController.dart';

import '../Screens/WishListScreen.dart';

class CustomAppbar extends StatelessWidget with PreferredSizeWidget {
  const CustomAppbar(
      {super.key,
      required this.media,
      required this.title,
      required this.allow,
      required this.sighn});
  final String title;
  final Size media;
  final bool allow;
  final bool sighn;

  @override
  Widget build(BuildContext context) {
    return AppBar(
        scrolledUnderElevation: 0,
        automaticallyImplyLeading: false,
        leading: allow
            ? IconButton(
                icon: const Icon(Icons.arrow_back_ios_new,
                    color: Colors.deepOrange),
                onPressed: () {
                  Get.back();
                },
              )
            : IconButton(
                icon: const Icon(
                    size: 30, Icons.logout_rounded, color: Colors.deepOrange),
                onPressed: () {
                  AuthController.instance.logupdate();
                  AuthController.instance.sighnOut();
                },
              ),
        centerTitle: true,
        actions: [
          !sighn
              ? IconButton(
                  onPressed: () {
                    Get.toNamed(WishListSC.routeName);
                  },
                  icon: const Icon(
                    Icons.favorite,
                    size: 30,
                    color: Colors.deepOrange,
                  ))
              : const SizedBox(
                  width: 10,
                ),
          const SizedBox(
            width: 10,
          )
        ],
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          title,
          style: Theme.of(context)
              .textTheme
              .headlineLarge!
              .copyWith(color: Colors.deepOrange),
        ));
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(media.height * 0.1);
}
