import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../controller/USerController.dart';
import '../../main.dart';

class CustomInput extends StatelessWidget {
  const CustomInput.normal({
    super.key,
    required this.hint,
    required this.title,
    required this.control,
    this.controller,
  });
  const CustomInput.user({
    super.key,
    required this.hint,
    required this.title,
    this.control,
    this.controller,
  });
  final String hint;
  final String title;
  final TextEditingController? control;
  final UserController? controller;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: medias(context).width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: Text(
              title,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ),
          SizedBox(
            width: medias(context).width * 0.7,
            height: medias(context).height * 0.1,
            child: TextFormField(
              inputFormatters: (title == 'age' || title == 'number')
                  ? <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                    ]
                  : [],
              validator: (value) {
                if (title != 'age') {
                  if (value!.length < 2) {
                    return "too short input";
                  }
                }
                return null;
              },
              onChanged: controller == null
                  ? (_) {}
                  : (value) {
                      controller!.newUser.update(
                        title,
                        (_) => value,
                        ifAbsent: () => title,
                      );
                    },
              controller: control,
              textAlign: TextAlign.start,
              textAlignVertical: TextAlignVertical.center,
              decoration: InputDecoration(
                  hintText: hint,
                  alignLabelWithHint: true,
                  focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  // enabledBorder: OutlineInputBorder(
                  //     borderSide: BorderSide(color: Colors.black)),
                  border: const OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black))),
            ),
          )
        ],
      ),
    );
  }
}
