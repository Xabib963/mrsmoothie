import 'package:flutter/material.dart';

class CategoryIntro extends StatelessWidget {
  const CategoryIntro({
    super.key,
    required this.title,
  });
  final title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5),
      child: Text(
        '$title :',
        style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 25),
      ),
    );
  }
}
