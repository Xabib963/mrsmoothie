import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';

import '../../main.dart';

class Trailingcart_Checout extends StatelessWidget {
  Trailingcart_Checout({
    super.key,
  });
  final CartController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(builder: (_) {
      return Column(
        children: [
          const Divider(
            thickness: 3,
            endIndent: 10,
            indent: 10,
            color: Colors.deepOrange,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Align(
                alignment: Alignment.center,
                child: Text(
                  "Sub total: ${controller.subtotal} \$",
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge!
                      .copyWith(fontSize: 18),
                  textAlign: TextAlign.start,
                )),
          ),
          Align(
              alignment: Alignment.center,
              child: Text(
                "Deleviry fee:${controller.deleviryfee.ceil()}\$",
                textAlign: TextAlign.start,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge!
                    .copyWith(fontSize: 18),
              )),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20),
            width: medias(context).width * 0.8,
            height: medias(context).height * 0.07,
            color: Colors.deepOrangeAccent[100],
            child: Container(
              margin: const EdgeInsets.all(5),
              color: Colors.deepOrange,
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Total :",
                      maxLines: 2,
                      overflow: TextOverflow.clip,
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(fontSize: 24),
                    ),
                    Text(
                      " ${controller.total}\$",
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(fontSize: 18),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    });
  }
}
