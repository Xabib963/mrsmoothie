import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:shopsy/controller/AuthController.dart';
import 'package:shopsy/controller/USerController.dart';
import 'package:shopsy/view/Screens/Addinformation.dart';

import '../../main.dart';

class LoginsScreen extends StatelessWidget {
  LoginsScreen({super.key});
  UserController _userController = Get.find();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  static const routeName = '/login';
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            width: medias(context).width,
            top: medias(context).height * 0.1,
            child: Lottie.asset(
              'assets/36196-fruit-basket.json',
            ),
          ),
          //if u want to blur the background un comment this part
          Positioned.fill(
              child: BackdropFilter(
                  child: SizedBox(),
                  filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5))),

          Positioned(
            width: medias(context).width,
            bottom: medias(context).height * 0.01,
            child: Lottie.asset(
              'assets/23228-orange-juice.json',
            ),
          ),

          Positioned(
              width: medias(context).width * 0.8,
              top: medias(context).height * 0.11,
              left: 40,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: medias(context).width,
                    child: const FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        maxLines: 2,
                        'Mr \nSmoothies',
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 50,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  const Divider(thickness: 2),
                  SizedBox(
                    width: medias(context).width,
                    child: Text(
                      maxLines: 5,
                      'Beer,naturals Drinks  and more than this in just one place\n mr Smoothies is the life discovring youpa',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: medias(context).width * 0.05,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              )),
          Positioned(
              width: medias(context).width * 0.8,
              top: medias(context).height * 0.6,
              left: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    elevation: 10,
                    fixedSize: Size(medias(context).width * 0.7,
                        medias(context).width * 0.15),
                    disabledBackgroundColor: Colors.transparent,
                    backgroundColor: Colors.deepOrange.withOpacity(0.8)),
                onPressed: () {
                  showDialog(
                      barrierDismissible: true,
                      barrierLabel: "hi",
                      context: context,
                      builder: (
                        context,
                      ) =>
                          Scaffold(
                            backgroundColor: Colors.transparent,
                            body: Center(
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white.withOpacity(0.95),
                                      borderRadius: BorderRadius.circular(20)),
                                  width: medias(context).width * 0.85,
                                  height: medias(context).height * 0.6,
                                  child: SafeArea(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SingleChildScrollView(
                                        child: SizedBox(
                                          width: medias(context).width * 0.85,
                                          height: medias(context).height * 0.6,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              GetBuilder<AuthController>(
                                                  builder: (controller) {
                                                return controller.sighnUp
                                                    ? const Text(
                                                        overflow:
                                                            TextOverflow.clip,
                                                        'Sign Up',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'Poppins',
                                                            fontSize: 35,
                                                            color: Colors
                                                                .deepOrange,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )
                                                    : const Text(
                                                        overflow:
                                                            TextOverflow.clip,
                                                        'Log In',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'Poppins',
                                                            fontSize: 35,
                                                            color: Colors
                                                                .deepOrange,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      );
                                              }),
                                              Form(
                                                key: formKey,
                                                child: SizedBox(
                                                  height:
                                                      medias(context).height *
                                                          0.23,
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal:
                                                                    20.0),
                                                        child: TextFormField(
                                                          validator: (value) {
                                                            if (!(value!
                                                                    .contains(
                                                                        '@')) ||
                                                                !(value.contains(
                                                                    '.com'))) {
                                                              return "Not a Valid Email Format";
                                                            }
                                                            return null;
                                                          },
                                                          controller:
                                                              _emailController,
                                                          decoration:
                                                              const InputDecoration(
                                                                  label: Text(
                                                                      'Email'),
                                                                  border:
                                                                      OutlineInputBorder()),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal:
                                                                    20.0),
                                                        child: TextFormField(
                                                          validator: (value) {
                                                            if (value!.length <
                                                                6) {
                                                              return "weak pass";
                                                            }
                                                            return null;
                                                          },
                                                          controller:
                                                              _passwordController,
                                                          decoration:
                                                              const InputDecoration(
                                                                  label: Text(
                                                                      'Password'),
                                                                  border:
                                                                      OutlineInputBorder()),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              GetBuilder<AuthController>(
                                                  builder: (controller) {
                                                return controller.loading
                                                    ? const SpinKitDualRing(
                                                        color:
                                                            Colors.deepOrange,
                                                      )
                                                    : controller.sighnUp
                                                        ? ElevatedButton(
                                                            style: ElevatedButton.styleFrom(
                                                                elevation: 10,
                                                                fixedSize: Size(
                                                                    medias(context).width *
                                                                        0.6,
                                                                    medias(context).width *
                                                                        0.15),
                                                                disabledBackgroundColor: Colors
                                                                    .transparent,
                                                                backgroundColor: Colors
                                                                    .deepOrange
                                                                    .withOpacity(
                                                                        0.8)),
                                                            onPressed: () {
                                                              if (formKey
                                                                  .currentState!
                                                                  .validate()) {
                                                                AuthController
                                                                    .instance
                                                                    .logupdate();
                                                                Get.toNamed(
                                                                    AdduserInfo
                                                                        .routeName,
                                                                    arguments: {
                                                                      'email':
                                                                          _emailController
                                                                              .text,
                                                                      'pass':
                                                                          _passwordController
                                                                              .text
                                                                    });
                                                              }
                                                            },
                                                            child: const Text(
                                                              overflow:
                                                                  TextOverflow
                                                                      .clip,
                                                              'Sign Up',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontSize: 20,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ))
                                                        : ElevatedButton(
                                                            style: ElevatedButton.styleFrom(
                                                                elevation: 10,
                                                                fixedSize: Size(
                                                                    medias(context).width *
                                                                        0.6,
                                                                    medias(context)
                                                                            .width *
                                                                        0.15),
                                                                disabledBackgroundColor:
                                                                    Colors.transparent,
                                                                backgroundColor: Colors.deepOrange.withOpacity(0.8)),
                                                            onPressed: () {
                                                              if (formKey
                                                                  .currentState!
                                                                  .validate()) {
                                                                AuthController
                                                                    .instance
                                                                    .logupdate();
                                                                AuthController.instance.loginwithemail(
                                                                    email: _emailController
                                                                        .text
                                                                        .trim(),
                                                                    password:
                                                                        _passwordController
                                                                            .text
                                                                            .trim());
                                                              }
                                                            },
                                                            child: const Text(
                                                              overflow:
                                                                  TextOverflow
                                                                      .clip,
                                                              'Log In',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontSize: 20,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ));
                                              }),
                                              const Row(
                                                children: [
                                                  Expanded(
                                                      child: Divider(
                                                          thickness: 2)),
                                                  Text(
                                                    overflow: TextOverflow.clip,
                                                    'or',
                                                    style: TextStyle(
                                                        fontFamily: 'Poppins',
                                                        fontSize: 20,
                                                        color: Colors.grey,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Expanded(
                                                      child: Divider(
                                                    thickness: 2,
                                                  )),
                                                ],
                                              ),
                                              GetBuilder<AuthController>(
                                                  builder: (control) {
                                                return control.sighnUp
                                                    ? TextButton(
                                                        onPressed: () {
                                                          control
                                                              .changToSignUp();
                                                        },
                                                        child: const Text(
                                                          overflow:
                                                              TextOverflow.clip,
                                                          'Login instade',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontSize: 20,
                                                              color: Colors
                                                                  .deepOrangeAccent,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      )
                                                    : TextButton(
                                                        onPressed: () {
                                                          control
                                                              .changToSignUp();
                                                        },
                                                        child: const Text(
                                                          overflow:
                                                              TextOverflow.clip,
                                                          'Register for Free',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontSize: 20,
                                                              color: Colors
                                                                  .deepOrangeAccent,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      );
                                              }),
                                              const SizedBox(
                                                height: 30,
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          ));
                },
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      overflow: TextOverflow.clip,
                      'Get Started',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Icon(
                      CupertinoIcons.arrow_right_circle,
                      color: Colors.white,
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
