import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:shopsy/view/Screens/HomeScreen.dart';
import 'package:shopsy/main.dart';
import 'package:shopsy/model/Product.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';
import 'package:shopsy/view/widget/ProductCard.dart';

import '../../controller/addtowishlistController.dart';

class WishListSC extends StatelessWidget {
  WishListSC({super.key});
  static const String routeName = '/wishlist';

  AddToWishlistController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        sighn: false,
        allow: true,
        media: medias(
          context,
        ),
        title: 'Wish List',
      ),
      body: SafeArea(
          child: GetBuilder<AddToWishlistController>(
              builder: (_) => SizedBox(
                    height: medias(context).height * 0.9,
                    child: ListView.builder(
                      padding: const EdgeInsets.all(6),
                      itemCount: controller.prods.length,
                      itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ProductCard.wishlist(
                          width: medias(context).width * 0.35,
                          height: medias(context).height * 0.35,
                          iswishlist: true,
                          media: medias(context),
                          addtoWishController: controller,
                          product: controller.prods[index],
                        ),
                      ),
                    ),
                  ))),
    );
  }
}
