import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:get/get.dart';
import 'package:shopsy/controller/AuthController.dart';
import '../../data/data_source/DatabaseServices.dart';

import '../../main.dart';

import '../widget/TrailingCart_checkout.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({super.key});
  static const routeName = '/profileScreen';
  DatabaseServices _dataSource = DatabaseServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: medias(context).height * 0.3,
            color: Colors.deepOrange,
            child: Image.asset(
              'assets/4105938-account-card-id-identification-identity-card-profile-user-profile_113929.png',
              height: medias(context).width * 0.4,
              color: Colors.white,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: medias(context).height * 0.57,
              child: FutureBuilder(
                  future: _dataSource
                      .getUserData(AuthController.instance.user!.email!),
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? ListView(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            shrinkWrap: true,
                            children: [
                              SizedBox(
                                height: medias(context).height * 0.03,
                              ),
                              Text(
                                "USER Details :",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                        fontSize: 20, color: Colors.grey[700]),
                              ),
                              const Divider(
                                thickness: 3,
                                endIndent: 10,
                                indent: 5,
                                color: Colors.deepOrange,
                              ),
                              SizedBox(
                                height: medias(context).height * 0.02,
                              ),
                              Text(
                                "Name :${snapshot.data!.name!.toUpperCase()} ",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                              SizedBox(
                                height: medias(context).height * 0.02,
                              ),
                              Text(
                                "User_Id :${snapshot.data!.id}",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.grey[700]),
                              ),
                              SizedBox(
                                height: medias(context).height * 0.05,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "age :${snapshot.data!.age}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 23,
                                            color: Colors.grey[700]),
                                  ),
                                  Text(
                                    "Job :${snapshot.data!.job}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 23,
                                            color: Colors.grey[700]),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: medias(context).height * 0.009,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: SizedBox(
                                  width: medias(context).width * 0.7,
                                  child: FittedBox(
                                    child: Text(
                                      maxLines: 1,
                                      "Address : ${snapshot.data!.address}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyLarge!
                                          .copyWith(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 23,
                                              color: Colors.grey[700]),
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: SizedBox(
                                  width: medias(context).width * 0.6,
                                  child: FittedBox(
                                    child: Text(
                                      maxLines: 1,
                                      "Phone : ${snapshot.data!.number}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyLarge!
                                          .copyWith(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 23,
                                              color: Colors.grey[700]),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: medias(context).height * 0.05,
                              ),
                              SizedBox(
                                width: medias(context).width * 0.4,
                                child: FittedBox(
                                  child: Text(
                                    maxLines: 1,
                                    "Email : ${snapshot.data!.email}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: Colors.grey[700]),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: medias(context).height * 0.005,
                              ),
                            ],
                          )
                        : const SpinKitDualRing(color: Colors.deepOrange);
                  }),
            ),
          )
        ],
      )),
      appBar: AppBar(
        scrolledUnderElevation: 0,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new, color: Colors.white),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: Colors.deepOrange,
        title: Text(
          'Profile',
          style: Theme.of(context)
              .textTheme
              .bodyLarge!
              .copyWith(color: Colors.white, fontSize: 30),
        ),
        centerTitle: true,
      ),
    );
  }
}
