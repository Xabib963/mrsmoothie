import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';
import 'package:shopsy/main.dart';
import 'package:shopsy/view/Screens/ConfirmationOrders.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';

import '../../controller/CheckOutController.dart';
import '../widget/Custominput.dart';
import '../widget/TextIntro.dart';
import '../widget/TrailingCart_checkout.dart';

class CheckoutScreen extends StatelessWidget {
  CheckoutScreen({super.key});
  static const routeName = "/checkout";

  CartController controller = Get.find();
  GlobalKey _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emaiController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: medias(context).width * 0.01,
              vertical: medias(context).width * 0.02),
          child: ElevatedButton(
            onPressed: () async {
              Get.toNamed(ConfirmationOrders.routeName, arguments: {
                'name': nameController.text,
                'email': emaiController.text,
                'city': cityController.text,
                'address': addressController.text,
              });
            },
            style: ElevatedButton.styleFrom(
                elevation: 5,
                backgroundColor: Colors.deepOrange,
                fixedSize: Size(medias(context).width * 0.4, 50)),
            child: FittedBox(
              child: Text(
                "Order Now >",
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.w900,
                    fontSize: 20),
              ),
            ),
          ),
        ),
        appBar: CustomAppbar(
            sighn: false,
            media: medias(context),
            title: "Check Out",
            allow: true),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 20),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Textintro(str: "CUSTOMER"),
                      SizedBox(
                        height: medias(context).height * 0.02,
                      ),
                      CustomInput.normal(
                        hint: "enter fullName",
                        title: "Full Name :",
                        control: nameController,
                      ),
                      SizedBox(
                        height: medias(context).height * 0.01,
                      ),
                      CustomInput.normal(
                          hint: "enter email",
                          title: "Email :",
                          control: emaiController),
                      SizedBox(
                        height: medias(context).height * 0.01,
                      ),
                      const Divider(
                        color: Colors.grey,
                        thickness: 2,
                      ),
                      SizedBox(
                        height: medias(context).height * 0.02,
                      ),
                      const Textintro(str: "DELIVERY"),
                      SizedBox(
                        height: medias(context).height * 0.02,
                      ),
                      CustomInput.normal(
                          control: addressController,
                          hint: "Address",
                          title: "Address"),
                      SizedBox(
                        height: medias(context).height * 0.01,
                      ),
                      CustomInput.normal(
                          control: cityController, hint: "City", title: "CITY"),
                      SizedBox(
                        height: medias(context).height * 0.01,
                      ),
                      const Divider(
                        color: Colors.grey,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: InkWell(
                          child: Container(
                            color: Colors.grey,
                            alignment: Alignment.center,
                            width: medias(context).width,
                            height: medias(context).height * 0.08,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "PAYMENT METHOD  ",
                                  style: Theme.of(context).textTheme.bodyLarge,
                                ),
                                Icon(Icons.arrow_forward_ios)
                              ],
                            ),
                          ),
                        ),
                      ),
                      Trailingcart_Checout()
                    ]),
              ),
            ),
          ),
        ));
  }
}
