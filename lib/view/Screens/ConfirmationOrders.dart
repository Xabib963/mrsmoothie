import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/AuthController.dart';
import '../../controller/Cartcontroller.dart';
import '../../controller/CheckOutController.dart';
import '../../main.dart';

import '../widget/TrailingCart_checkout.dart';

class ConfirmationOrders extends StatelessWidget {
  ConfirmationOrders({super.key});
  static const routeName = '/confirmationOrder';
  CheckoutController checkController = Get.find();
  Map<String, dynamic> args = Get.arguments;
  CartController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: medias(context).width * 0.01,
            vertical: medias(context).width * 0.02),
        child: ElevatedButton(
          onPressed: () async {
            print(args);
            await checkController
                .sendData(
                    args['name'],
                    args['email'],
                    args['city'],
                    args['address'],
                    controller.prods.map((e) => e.name.toString()).toList())
                .then(
                    (value) => Get.snackbar(value ?? 'done', value ?? 'done'));
          },
          style: ElevatedButton.styleFrom(
              elevation: 5,
              backgroundColor: Colors.deepOrange,
              fixedSize: Size(medias(context).width * 0.4, 50)),
          child: FittedBox(
            child: Text(
              maxLines: 1,
              "Confirm ",
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.w900,
                  fontSize: 24),
            ),
          ),
        ),
      ),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: medias(context).height * 0.3,
            color: Colors.deepOrange,
            child: SvgPicture.asset(
              'assets/order_confirmation_icon_181878.svg',
              height: medias(context).width * 0.5,
              color: Colors.white,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: medias(context).height * 0.57,
              child: ListView(
                shrinkWrap: true,
                children: [
                  SizedBox(
                    height: medias(context).height * 0.02,
                  ),
                  Text(
                    "ORDER_CODE : #E22B30N1.5e6",
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 20, color: Colors.grey[700]),
                  ),
                  SizedBox(
                    height: medias(context).height * 0.03,
                  ),
                  Text(
                    "Thnx ${AuthController.instance.user!.email!.split('@')[0]} you'r order Done",
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontSize: 17),
                  ),
                  SizedBox(
                    height: medias(context).height * 0.02,
                  ),
                  Trailingcart_Checout(),
                  SizedBox(
                    height: medias(context).height * 0.005,
                  ),
                  Text(
                    "ORDER Details :",
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(fontSize: 20, color: Colors.grey[700]),
                  ),
                  const Divider(
                    thickness: 3,
                    endIndent: 10,
                    indent: 10,
                    color: Colors.deepOrange,
                  ),
                  ListView.builder(
                    padding: const EdgeInsets.only(bottom: 30),
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: controller.prods.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.network(
                              fit: BoxFit.fill,
                              controller.prods[index].imgUrl,
                              width: 70,
                              errorBuilder: (context, error, stackTrace) =>
                                  Container(
                                      width: 100,
                                      height: 100,
                                      alignment: Alignment.center,
                                      child: const Icon(Icons.error_outline,
                                          color: Colors.red)),
                              height: 50,
                            ),
                            SizedBox(
                              width: medias(context).width * 0.5,
                              height: medias(context).height * 0.09,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                    " ${controller.prods[index].name}  #$index ",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(fontSize: 18),
                                  ),
                                  Text(
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 3,
                                    textAlign: TextAlign.center,
                                    "${controller.prods[index].quantity}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(fontSize: 15),
                                  ),
                                  const SizedBox()
                                ],
                              ),
                            ),
                            SizedBox(
                              width: medias(context).width * 0.18,
                              child: Text(
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                " ${controller.prods[index].price} \$ ",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .copyWith(fontSize: 18),
                              ),
                            ),
                          ]),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      )),
      appBar: AppBar(
        scrolledUnderElevation: 0,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new, color: Colors.white),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: Colors.deepOrange,
        title: Text(
          'Confirmation',
          style: Theme.of(context)
              .textTheme
              .bodyLarge!
              .copyWith(color: Colors.white, fontSize: 30),
        ),
        centerTitle: true,
      ),
    );
  }
}
