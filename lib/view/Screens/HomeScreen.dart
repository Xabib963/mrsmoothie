import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/Homedatacontroller.dart';

import 'package:shopsy/main.dart';

import 'package:shopsy/view/widget/CarouselCard.dart';

import '../../data/data_source/DatabaseServices.dart';
import '../widget/Categoryintro.dart';
import '../widget/CostumAppbar.dart';
import '../widget/CustomNavBar.dart';
import '../widget/ProductCard.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({super.key});
  static const String routeName = '/Home';

  DatabaseServices _databaseServices = DatabaseServices();
  HomedataController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CustomBottomNaveBar(media: medias(context)),
      appBar: CustomAppbar(
          sighn: false,
          allow: false,
          media: medias(context),
          title: 'Appologies'),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            FutureBuilder(
                future: _databaseServices.getCatiguries(),
                builder: (context, snapshot) {
                  return snapshot.hasData
                      ? CarouselSlider(
                          options: CarouselOptions(
                            aspectRatio: 2,
                            viewportFraction: 0.8,
                            enlargeStrategy: CenterPageEnlargeStrategy.height,
                            enlargeCenterPage: true,
                            initialPage: 2,
                          ),
                          items: snapshot.data!
                              .map((e) => CarouselCard(cate: e))
                              .toList())
                      : SizedBox(
                          height: medias(context).height * 0.28,
                          child: const SpinKitDoubleBounce(
                            color: Colors.deepOrange,
                          ),
                        );
                }),
            SizedBox(
              height: medias(context).height * 0.58,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const CategoryIntro(title: "All Products"),
                    FutureBuilder(
                      initialData: [],
                      future: _databaseServices.getProducts(),
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? SizedBox(
                                height: medias(context).height * 0.31,
                                child: GridView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: snapshot.data!.length,
                                    padding: const EdgeInsets.all(8),
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                            childAspectRatio: 0.75 / 1,
                                            crossAxisSpacing: 10,
                                            mainAxisSpacing: 10,
                                            crossAxisCount: 2),
                                    itemBuilder: (context, index) {
                                      return ProductCard.catalog(
                                        height: medias(context).height * 0.35,
                                        iswishlist: false,
                                        width: medias(context).width * 0.45,
                                        product: snapshot.data![index],
                                        media: medias(context),
                                      );
                                    }),
                              )
                            : SizedBox(
                                height: medias(context).width * 0.2,
                                child: const SpinKitDoubleBounce(
                                  color: Colors.deepOrange,
                                ),
                              );
                      },
                    ),
                    const CategoryIntro(title: "Trendy "),
                    StreamBuilder(
                        initialData: [],
                        stream: _databaseServices.getTrendyProducts(),
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? Container(
                                  margin: const EdgeInsets.all(5),
                                  width: double.infinity,
                                  height: medias(context).width * 0.35,
                                  child: ListView.builder(
                                    itemCount: snapshot.data!.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) => Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: ProductCard.prduct(
                                        height: medias(context).height * 0.33,
                                        iswishlist: false,
                                        width: medias(context).width * 0.45,
                                        product: snapshot.data![index],
                                        media: medias(context),
                                      ),
                                    ),
                                  ))
                              : SizedBox(
                                  height: medias(context).width * 0.32,
                                  child: const SpinKitDoubleBounce(
                                    color: Colors.deepOrange,
                                  ),
                                );
                        }),
                    const CategoryIntro(title: "Recomended"),
                    StreamBuilder(
                        initialData: [],
                        stream: _databaseServices.getrecommendedProducts(),
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? Container(
                                  margin: const EdgeInsets.all(5),
                                  width: double.infinity,
                                  height: medias(context).width * 0.35,
                                  child: ListView.builder(
                                    itemCount: snapshot.data!.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) => Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: ProductCard.prduct(
                                        height: medias(context).height * 0.35,
                                        iswishlist: false,
                                        width: medias(context).width * 0.45,
                                        product: snapshot.data![index],
                                        media: medias(context),
                                      ),
                                    ),
                                  ))
                              : SizedBox(
                                  height: medias(context).width * 0.35,
                                  child: const SpinKitDoubleBounce(
                                    color: Colors.deepOrange,
                                  ),
                                );
                        }),
                    SizedBox(
                      height: medias(context).height * 0.15,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
