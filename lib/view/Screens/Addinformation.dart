import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/AuthController.dart';
import 'package:shopsy/controller/USerController.dart';
import 'package:shopsy/main.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';
import 'package:shopsy/view/widget/Custominput.dart';

import '../../data/data_source/DatabaseServices.dart';

class AdduserInfo extends StatelessWidget {
  AdduserInfo({super.key});
  static const routeName = '/AddUserinfo';
  Map<String, dynamic> email_pass = Get.arguments;

  DatabaseServices _databaseServices = DatabaseServices();
  UserController _userController = Get.find();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
          sighn: true,
          media: medias(context),
          title: "Add your Info",
          allow: true),
      body: SafeArea(
        child: Column(children: [
          Form(
            key: _formKey,
            child: Column(
              children: [
                Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10),
                    child: CustomInput.user(
                        controller: _userController,
                        hint: 'Full Name',
                        title: 'Name')),
                Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10),
                    child: CustomInput.user(
                        controller: _userController,
                        hint: 'Adress',
                        title: 'address')),
                Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10),
                    child: CustomInput.user(
                        controller: _userController,
                        hint: 'Age',
                        title: 'age')),
                Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10),
                    child: CustomInput.user(
                        controller: _userController,
                        hint: 'job title',
                        title: 'job')),
                Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10),
                    child: CustomInput.user(
                        controller: _userController,
                        hint: 'Phone Number',
                        title: 'number')),
              ],
            ),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  elevation: 10,
                  fixedSize: Size(medias(context).width * 0.6,
                      medias(context).width * 0.15),
                  disabledBackgroundColor: Colors.transparent,
                  backgroundColor: Colors.deepOrange.withOpacity(0.8)),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _databaseServices.addUser({
                    'email': email_pass['email'],
                    'Name': _userController.newUser["Name"],
                    'address': _userController.newUser["address"],
                    'age': _userController.newUser["age"],
                    'job': _userController.newUser["job"],
                    'number': _userController.newUser["number"],
                  });
                  AuthController.instance.register(
                      email: email_pass['email'].trim(),
                      password: email_pass['pass'].trim());
                }
              },
              child: const Text(
                overflow: TextOverflow.clip,
                'Sign Up',
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Poppins',
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ))
        ]),
      ),
    );
  }
}
