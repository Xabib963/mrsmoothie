import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';

import 'package:shopsy/main.dart';
import 'package:shopsy/model/Cartpromodel.dart';
import 'package:shopsy/model/Product.dart';
import 'package:shopsy/view/widget/CarouselCard.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';

import '../../controller/addtowishlistController.dart';

class ProductScreen extends StatelessWidget {
  ProductScreen({
    super.key,
  });
  Product product = Get.arguments;
  static const String routeName = '/Product';
  AddToWishlistController controller = Get.find();
  CartController cartController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        sighn: false,
        allow: true,
        title: product.name,
        media: medias(context),
      ),
      bottomNavigationBar: Container(
        width: medias(context).width,
        height: medias(context).height * 0.075,
        color: Colors.deepOrange,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  size: 29,
                  Icons.share,
                  color: Colors.white,
                )),
            IconButton(
                onPressed: () async {
                  // print(product);
                  await controller.addtoWishlist(product);
                  Get.snackbar("Added Succesfuly", 'done',
                      snackStyle: SnackStyle.FLOATING,
                      messageText: Text(
                        "Product added to wishlist",
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge!
                            .copyWith(color: Colors.black),
                      ),
                      titleText: Text(
                        "Added Succesfuly",
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge!
                            .copyWith(color: Colors.green),
                      ),
                      snackPosition: SnackPosition.BOTTOM);
                },
                icon: const Icon(
                  size: 29,
                  Icons.favorite,
                  color: Colors.white,
                )),
            ActionChip(
              onPressed: () => cartController.addtoCart(
                  Cartproduct(product.name, product.imgUrl, product.price!)),
              labelStyle: Theme.of(context)
                  .textTheme
                  .bodyLarge!
                  .copyWith(color: Colors.deepOrange),
              label: const Text(
                "Add To Cart",
              ),
              padding: EdgeInsets.symmetric(
                  horizontal: medias(context).width * 0.15,
                  vertical: medias(context).width * 0.0250),
            )
          ],
        ),
      ),
      body: SafeArea(
          child: Column(
        children: [
          SizedBox(
            height: medias(context).height * 0.3,
            child: CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2,
                viewportFraction: 0.8,
                enlargeStrategy: CenterPageEnlargeStrategy.height,
                enlargeCenterPage: true,
                initialPage: 2,
              ),
              items: [
                CarouselCard(product: product),
              ],
            ),
          ),
          Container(
            width: medias(context).width * 0.8,
            height: medias(context).height * 0.1,
            color: Colors.deepOrangeAccent.withOpacity(0.7),
            child: Container(
              margin: const EdgeInsets.all(5),
              color: Colors.deepOrange,
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      product.name!,
                      maxLines: 2,
                      overflow: TextOverflow.clip,
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(fontSize: 24),
                    ),
                    Text(
                      "${product.price}\$",
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(fontSize: 20),
                    )
                  ],
                ),
              ),
              // Expanded(
              //     flex: 1,
              //     child: IconButton(
              //         onPressed: () {},
              //         icon: Icon(
              //             size: medias(context).width * 0.12,
              //             color: Colors.white,
              //             Icons.add_circle))),
            ),
          ),
          SizedBox(
            height: medias(context).height * 0.39,
            child: SingleChildScrollView(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 22.0, vertical: 15),
                child: Column(
                  children: [
                    ExpansionTile(
                      backgroundColor: Colors.grey.withOpacity(0.15),
                      collapsedBackgroundColor: Colors.grey.withOpacity(0.2),
                      initiallyExpanded: true,
                      childrenPadding:
                          const EdgeInsets.symmetric(horizontal: 10.0),
                      shape:
                          const RoundedRectangleBorder(side: BorderSide.none),
                      title: Text("Product Infos",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(
                                  color: Colors.deepOrange, fontSize: 20)),
                      children: [
                        Text(
                            style: Theme.of(context)
                                .textTheme
                                .bodySmall!
                                .copyWith(color: Colors.black, fontSize: 16),
                            "${product.description}\$")
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 22.0, vertical: 15),
                      child: ExpansionTile(
                        backgroundColor: Colors.grey.withOpacity(0.15),
                        collapsedBackgroundColor: Colors.grey.withOpacity(0.2),
                        initiallyExpanded: true,
                        childrenPadding:
                            const EdgeInsets.symmetric(horizontal: 10.0),
                        shape: const RoundedRectangleBorder(
                            side: BorderSide(
                          color: Colors.black,
                        )),
                        title: Text(
                          "Deleviry Infos",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(color: Colors.deepOrange, fontSize: 20),
                        ),
                        children: [
                          Text(
                              style: Theme.of(context)
                                  .textTheme
                                  .bodySmall!
                                  .copyWith(color: Colors.black, fontSize: 16),
                              "Occaecat adipisicing do sunt est fugiat officia fugiat ad consequat. Cillum laboris enim pariatur dolore nulla ipsum ex adipisicing ex sit ea dolore qui fugiat. Officia do tempor enim in sint ea. Pariatur sit anim duis amet non eu minim sunt ex tempor dolor aliquip culpa esse. Eiusmod esse ut aute tempor amet officia labore amet consectetur esse.")
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }
}
