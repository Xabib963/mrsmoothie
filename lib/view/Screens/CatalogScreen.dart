import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:shopsy/model/Catigory.dart';
import 'package:shopsy/model/Product.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';
import 'package:shopsy/view/widget/ProductCard.dart';

import '../../data/data_source/DatabaseServices.dart';
import '../../main.dart';
import 'HomeScreen.dart';

class CatalogScreen extends StatelessWidget {
  CatalogScreen({super.key});
  Catigory cate = Get.arguments;
  static const String routeName = '/catalog';
  DatabaseServices _databaseServices = DatabaseServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        sighn: false,
        media: medias(context),
        title: cate.name,
        allow: true,
      ),
      body: SafeArea(
          child: SizedBox(
        height: medias(context).height * 0.8,
        width: double.infinity,
        child: StreamBuilder<List<Product>>(
            initialData: [],
            stream: _databaseServices.getProductsbyCate(cate.name ?? 'khara'),
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? GridView.builder(
                      padding: const EdgeInsets.all(5),
                      itemCount: snapshot.data!.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 1.9 / 1.45,
                              crossAxisSpacing: 5,
                              mainAxisSpacing: 5),
                      itemBuilder: (context, index) => ProductCard.catalog(
                        height: medias(context).height * 0.35,
                        width: medias(context).height * 0.45,
                        iswishlist: false,
                        media: medias(context),
                        product: snapshot.data![index],
                      ),
                    )
                  : SizedBox(
                      height: medias(context).width * 0.35,
                      child: const SpinKitDoubleBounce(
                        color: Colors.deepOrange,
                      ),
                    );
            }),
      )),
    );
  }
}
