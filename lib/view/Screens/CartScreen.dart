import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';
import 'package:shopsy/main.dart';
import 'package:shopsy/model/Cartpromodel.dart';
import 'package:shopsy/view/Screens/checkoutScreen.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';

import '../widget/CartWidget.dart';
import '../widget/TrailingCart_checkout.dart';

class CartScreen extends StatelessWidget {
  CartScreen({super.key});
  static const String routeName = '/cartscreen';
  CartController cartController = Get.find();
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: medias(context).width * 0.01,
            vertical: medias(context).width * 0.02),
        child: ElevatedButton(
          onPressed: () async {
            Get.toNamed(
              CheckoutScreen.routeName,
            );
          },
          style: ElevatedButton.styleFrom(
              elevation: 5,
              backgroundColor: Colors.deepOrange,
              fixedSize: Size(medias(context).width * 0.6, 60)),
          child: FittedBox(
            child: Text(
              maxLines: 1,
              "Go To Checkout",
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.w900,
                  fontSize: 20),
            ),
          ),
        ),
      ),
      appBar: CustomAppbar(
        sighn: false,
        media: media,
        title: 'Cart',
        allow: true,
      ),
      body: SafeArea(
          child: Column(
        children: [
          GetBuilder<CartController>(builder: (controller) {
            return SizedBox(
              height: medias(context).height * 0.53,
              child: ListView.builder(
                itemCount: controller.prods.length,
                itemBuilder: (context, index) =>
                    Cartwid(prod: controller.prods[index]),
              ),
            );
          }),
          Trailingcart_Checout(),
          const SizedBox(
            height: 50,
          )
        ],
      )),
    );
  }
}
