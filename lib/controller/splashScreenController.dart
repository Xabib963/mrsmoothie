import 'package:get/get.dart';
import 'package:shopsy/view/Screens/HomeScreen.dart';

class SplashScreenController extends GetxController {
  bool showSpin = false;
  show() {
    showSpin = true;
    update();
  }
}
