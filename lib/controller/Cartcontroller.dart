import 'package:get/get.dart';
import 'package:shopsy/model/Cartpromodel.dart';
import 'package:shopsy/model/Product.dart';
import 'package:shopsy/model/Wishlist.dart';

class CartController extends GetxController {
  List<Cartproduct> prods = [];
  double total = 0;
  double subtotal = 0;
  double deleviryfee = 0;
  addtoCart(Cartproduct p) {
    prods.add(p);
    print(prods);
    update();
  }

  removeFromcart(Cartproduct p) {
    prods.remove(p);
    update();
  }

  add(Cartproduct p) {
    p.quantity = p.quantity + 1;
    total = total + p.price;
    if (total > 15) {
      subtotal = total - (p.price - 4);
      deleviryfee = total - (p.price * 4);
      update();
    } else {
      update();
    }
  }

  remove(Cartproduct p) {
    int v = p.quantity - 1;
    p.quantity = v;
    total = total - p.price;
    if (total < 15 && total > 6) {
      subtotal = total - 3;
      deleviryfee = total - 5;
      update();
    } else {
      subtotal = 0;
      deleviryfee = 0;
      update();
    }
    update();
  }

  // gettotlal() {
  //   if (prods.isEmpty) {
  //     return total;
  //   } else {
  //     prods.map((e) {
  //       total = total + e.price;
  //     });
  //     print(total);

  //     return total;
  //   }
  // }
}
