import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopsy/view/Screens/HomeScreen.dart';
import 'package:shopsy/view/Screens/LoginScreen.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  late Rx<User?> _user;
  bool loading = false;
  void logupdate() {
    loading = !loading;
    update();
  }

  User? get user => auth.currentUser;
  FirebaseAuth auth = FirebaseAuth.instance;
  bool sighnUp = false;
  void changToSignUp() {
    sighnUp = !sighnUp;
    update();
  }

  @override
  void onReady() {
    super.onReady();

    _user = Rx<User?>(auth.currentUser);
    _user.bindStream(auth.userChanges());
    ever(_user, initialScreen);
  }

  initialScreen(User? user) {
    if (user == null) {
      Get.offAllNamed(LoginsScreen.routeName);
    } else {
      Get.offAllNamed(MyHomePage.routeName);
    }
  }

  void loginwithemail({required String email, required String password}) async {
    try {
      await auth.signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        Get.snackbar("user-not-found", "Try Again",
            backgroundColor: Colors.red.withOpacity(0.7));
      } else if (e.code == 'wrong-password') {
        Get.snackbar("wrong-password", "Try Again",
            backgroundColor: Colors.red.withOpacity(0.7));
      }
    }
  }

  void register({required String email, required String password}) async {
    try {
      await auth.createUserWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Get.snackbar("Try Again", "weak-password",
            backgroundColor: Colors.red.withOpacity(0.7));
      } else if (e.code == 'email-already-in-use') {
        Get.snackbar("email-already-in-use", "Try again",
            backgroundColor: Colors.red.withOpacity(0.7));
      }
    } catch (e) {
      Get.snackbar("Try Again", "Sign Up failed", backgroundColor: Colors.red);

      print(e);
    }
  }

  Future<void> sighnOut() async {
    await auth.signOut();
    throw UnimplementedError();
  }
}
