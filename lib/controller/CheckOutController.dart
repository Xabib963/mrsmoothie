import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shopsy/controller/Cartcontroller.dart';
import 'package:shopsy/main.dart';
import '../model/Cartpromodel.dart';

class CheckoutController extends GetxController {
  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  Future sendData(String name, String email, String city, String address,
      List<String> product) async {
    firebaseFirestore.collection('checkouts').add({
      "name": name,
      "email": email,
      "address": address,
      "city": city,
      "products": product
    });
  }
}
