import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:shopsy/data/storageLocal/LocalStorgeRepositery.dart';
import 'package:shopsy/model/Product.dart';
import 'package:shopsy/model/Wishlist.dart';

class AddToWishlistController extends GetxController {
  late Box box;
  var prods = [];
  @override
  void onInit() async {
    box = LocalStorageRepos.getBox();
    prods = LocalStorageRepos.getWishlist(box);
    update();
    super.onInit();
  }

  addtoWishlist(Product p) async {
    print(p);

    print(box.values);
    await LocalStorageRepos.addProductToWishList(box, p);
    prods = LocalStorageRepos.getWishlist(box);
    update();
  }

  removeFromWishlist(Product p) async {
    LocalStorageRepos.removeProductFromWishList(box, p);
    prods = LocalStorageRepos.getWishlist(box);

    update();
  }
}
