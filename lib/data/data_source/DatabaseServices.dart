import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shopsy/model/UserInfo.dart';

import '../../model/Catigory.dart';

import '../../model/Product.dart';

class DatabaseServices {
  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  Future<List<Product>> getProducts() async {
    QuerySnapshot<Map<String, dynamic>> data =
        await firebaseFirestore.collection('products').get();

    List<Product> data1 = data.docs.map((e) {
      return Product.fromSnap(e);
    }).toList();

    return data1;
  }

  Stream<List<Product>> getTrendyProducts() {
    Stream<QuerySnapshot<Map<String, dynamic>>> data = firebaseFirestore
        .collection('products')
        .where('istrendy', isEqualTo: true)
        .snapshots();

    var data1 =
        data.map((e1) => e1.docs.map((e) => Product.fromSnap(e)).toList());

    return data1;
  }

  Stream<List<Product>> getProductsbyCate(String cate) {
    Stream<QuerySnapshot<Map<String, dynamic>>> data = firebaseFirestore
        .collection('products')
        .where('category', isEqualTo: cate)
        .snapshots();

    var data1 =
        data.map((e1) => e1.docs.map((e) => Product.fromSnap(e)).toList());

    return data1;
  }

  Stream<List<Product>> getrecommendedProducts() {
    Stream<QuerySnapshot<Map<String, dynamic>>> data = firebaseFirestore
        .collection('products')
        .where('isrecommended', isEqualTo: true)
        .snapshots();

    var data1 =
        data.map((e1) => e1.docs.map((e) => Product.fromSnap(e)).toList());

    return data1;
  }

  Future<List<Catigory>> getCatiguries() {
    return firebaseFirestore.collection('categories').get().then((value) {
      return value.docs.map((e) => Catigory.fromSnap(e)).toList();
    });
  }

  Future<UserInfo> getUserData(String email) async {
    var data = await firebaseFirestore
        .collection('users')
        .where('email', isEqualTo: 'habib@gmail.com')
        .get();

    return UserInfo.fromJson(data.docs.first);
  }

  Future addUser(user) async {
    return firebaseFirestore.collection('users').add(user);
  }
}
