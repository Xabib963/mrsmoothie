import 'package:hive/hive.dart';

import 'package:shopsy/model/Product.dart';

class LocalStorageRepos {
  static const String boxName = 'wishList';

  static openBox() async {
    Box box = await Hive.openBox<Product>(boxName);
    return box;
  }

  static Box getBox() => Hive.box<Product>(boxName);

  static List<Product> getWishlist(Box box) {
    return box.values.toList() as List<Product>;
  }

  static clearWishlist(Box box) async {
    await box.clear();
  }

  static removeProductFromWishList(Box box, Product product) async {
    await box.delete(product.id);
  }

  static addProductToWishList(Box box, Product product) async {
    await box.put(product.id, product);
  }
}
