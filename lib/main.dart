import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:shopsy/Settings/Approuter.dart';
import 'package:shopsy/model/Product.dart';
import 'package:shopsy/view/Screens/LoginScreen.dart';
import 'package:shopsy/view/widget/CostumAppbar.dart';

import 'Settings/Binding/splashBinding.dart';
import 'Settings/SplashScreen.dart';
import 'data/storageLocal/LocalStorgeRepositery.dart';
import 'controller/addAndRemoveproduct.dart';
import 'controller/Cartcontroller.dart';
import 'controller/addtowishlistController.dart';
import 'view/Screens/HomeScreen.dart';
import 'Settings/Theme.dart';

String url = "https://smoothies-991ef-default-rtdb.firebaseio.com/";
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await Hive.initFlutter();
  Hive.registerAdapter(ProductAdapter());
  await LocalStorageRepos.openBox();

  runApp(MyApp());
}

Size medias(BuildContext context) {
  return MediaQuery.of(context).size;
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: LoginsScreen.routeName,
      title: 'Mr.Smoothies',
      theme: theme(),
      initialBinding: SplashBinding(),
      getPages: appRoutes(),
    );
  }
}
