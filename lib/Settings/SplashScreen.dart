import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:lottie/lottie.dart';
import 'package:get/get.dart';
import 'package:shopsy/controller/splashScreenController.dart';
import 'package:shopsy/main.dart';
import 'package:shopsy/view/Screens/HomeScreen.dart';

class SplashScreen extends StatelessWidget {
  SplashScreen({super.key});
  static const routeName = '/';
  SplashScreenController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Mr. Smothiee",
            style: Theme.of(context)
                .textTheme
                .bodyLarge!
                .copyWith(color: Colors.deepOrange, fontSize: 40),
          ),
          Lottie.asset(
            delegates: LottieDelegates(
              values: [
                ValueDelegate.colorFilter(
                  ['57 - Cocktail Glass', '**'],
                  value: ColorFilter.mode(Colors.blue, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Line5', '**'],
                  value: ColorFilter.mode(Colors.green, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Line4', '**'],
                  value: ColorFilter.mode(Colors.green, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Line3', '**'],
                  value: ColorFilter.mode(Colors.green, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Line2', '**'],
                  value: ColorFilter.mode(Colors.green, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Line1', '**'],
                  value: ColorFilter.mode(Colors.deepOrange, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Stem Outlines', '**'],
                  value:
                      ColorFilter.mode(Colors.deepOrangeAccent, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Liquid Outlines', '**'],
                  value: ColorFilter.mode(Colors.white, BlendMode.src),
                ),
                ValueDelegate.colorFilter(
                  ['Coupe1 Outlines', '**'],
                  value: ColorFilter.mode(Colors.deepOrange, BlendMode.src),
                ),
              ],
            ),
            // delegates: LottieDelegates(
            //   values: [
            //     ValueDelegate.color(
            //       // keyPath order: ['layer name', 'group name', 'shape name']
            //       const ['**', '', '**'],
            //       value: Colors.orange,
            //     ),
            //   ],
            // ),
            'assets/12755-cocktail-glass.json',
          ),
          GetBuilder<SplashScreenController>(builder: (controller1) {
            return controller1.showSpin
                ? const Center(
                    child: SpinKitFadingCube(color: Colors.deepOrange),
                  )
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        fixedSize: Size(medias(context).width * 0.4, 60),
                        backgroundColor: Colors.deepOrange),
                    child: Text(
                      "Login",
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                    onPressed: () async {
                      controller.show();
                      await Future.delayed(Duration(seconds: 2));
                      Get.offAllNamed(MyHomePage.routeName);
                    },
                  );
          })
        ],
      )),
    );
  }
}
