import 'package:flutter/material.dart';

ThemeData theme() {
  return ThemeData(
      useMaterial3: true,
      primaryColor: Colors.deepOrange,
      canvasColor: Colors.white,
      fontFamily: 'Avenir',
      textTheme: textTheme());
}

TextTheme textTheme() {
  return TextTheme(
      bodyLarge:
          TextStyle(color: Colors.grey[800], fontWeight: FontWeight.bold),
      bodyMedium:
          TextStyle(color: Colors.grey[800], fontWeight: FontWeight.normal),
      bodySmall:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
      headlineLarge:
          const TextStyle(color: Colors.white, fontWeight: FontWeight.bold));
}
