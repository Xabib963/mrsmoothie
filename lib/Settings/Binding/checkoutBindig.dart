import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';

import '../../controller/CheckOutController.dart';
import '../../controller/Homedatacontroller.dart';

class CheckoutBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(CartController(), permanent: true);
    Get.put(
      CheckoutController(),
    );
  }
}
