import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';
import 'package:shopsy/controller/addtowishlistController.dart';

class ProductdaitelsBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(AddToWishlistController(), permanent: true);
    Get.put(CartController(), permanent: true);
  }
}
