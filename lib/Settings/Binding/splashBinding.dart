import 'package:get/get.dart';
import 'package:shopsy/controller/AuthController.dart';
import 'package:shopsy/controller/USerController.dart';
import 'package:shopsy/controller/splashScreenController.dart';

import '../../controller/Cartcontroller.dart';

class SplashBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(SplashScreenController());
    Get.put(AuthController());
    Get.put(UserController());
  }
}
