import 'package:get/get.dart';
import 'package:shopsy/controller/USerController.dart';

class AddUserBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(UserController(), permanent: true);
  }
}
