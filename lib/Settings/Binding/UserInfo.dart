import 'package:get/get.dart';

import '../../controller/USerController.dart';

class UserBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(UserController());
  }
}
