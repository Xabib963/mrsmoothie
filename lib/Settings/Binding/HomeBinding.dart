import 'package:get/get.dart';
import 'package:shopsy/controller/Cartcontroller.dart';

import '../../controller/Homedatacontroller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(CartController(), permanent: true);
    Get.put(HomedataController(), permanent: true);
  }
}
