import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:shopsy/Settings/Binding/AddTowishListBinding.dart';
import 'package:shopsy/Settings/Binding/HomeBinding.dart';
import 'package:shopsy/Settings/Binding/checkoutBindig.dart';
import 'package:shopsy/Settings/SplashScreen.dart';
import 'package:shopsy/view/Screens/CatalogScreen.dart';
import 'package:shopsy/view/Screens/ConfirmationOrders.dart';
import 'package:shopsy/view/Screens/LoginScreen.dart';
import 'package:shopsy/view/Screens/Productscreen.dart';
import 'package:shopsy/view/Screens/WishListScreen.dart';
import 'package:shopsy/view/Screens/checkoutScreen.dart';

import '../view/Screens/Addinformation.dart';
import '../view/Screens/CartScreen.dart';
import '../view/Screens/HomeScreen.dart';
import '../view/Screens/Profile.dart';
import 'Binding/AddUserBindings.dart';
import 'Binding/UserInfo.dart';

appRoutes() => [
      GetPage(
        name: SplashScreen.routeName,
        page: () => SplashScreen(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: LoginsScreen.routeName,
        page: () => LoginsScreen(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: ProfileScreen.routeName,
        page: () => ProfileScreen(),
        binding: UserBindings(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: MyHomePage.routeName,
        page: () => MyHomePage(),
        binding: HomeBinding(),
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: WishListSC.routeName,
        page: () => WishListSC(),
        binding: ProductdaitelsBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: ProductScreen.routeName,
        page: () => ProductScreen(),
        binding: ProductdaitelsBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: CheckoutScreen.routeName,
        page: () => CheckoutScreen(),
        binding: CheckoutBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: CatalogScreen.routeName,
        page: () => CatalogScreen(),
        binding: ProductdaitelsBinding(),
        middlewares: [MyMiddelware()],
        transition: Transition.circularReveal,
        transitionDuration: Duration(milliseconds: 1000),
      ),
      GetPage(
        name: CartScreen.routeName,
        page: () => CartScreen(),
        middlewares: [MyMiddelware()],
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: ConfirmationOrders.routeName,
        page: () => ConfirmationOrders(),
        middlewares: [MyMiddelware()],
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 500),
      ),
      GetPage(
        name: AdduserInfo.routeName,
        page: () => AdduserInfo(),
        binding: AddUserBindings(),
        middlewares: [MyMiddelware()],
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 500),
      ),
    ];

class MyMiddelware extends GetMiddleware {
  @override
  GetPage? onPageCalled(GetPage? page) {
    print(page?.name);
    return super.onPageCalled(page);
  }
}
