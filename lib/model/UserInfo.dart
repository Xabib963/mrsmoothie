import 'package:cloud_firestore/cloud_firestore.dart';

class UserInfo {
  String? email;
  String? name;
  String? address;
  String? age;
  String? job;
  String? number;
  String? id;

  UserInfo(
      {this.email, this.name, this.address, this.age, this.job, this.number});

  UserInfo.fromJson(QueryDocumentSnapshot<Map<String, dynamic>> json) {
    email = json['email'];
    name = json['Name'];
    address = json['address'];
    age = json['age'];
    job = json['job'];
    number = json['number'];
    id = json.id;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['Name'] = this.name;
    data['address'] = this.address;
    data['age'] = this.age;
    data['job'] = this.job;
    data['number'] = this.number;
    return data;
  }
}
