import 'package:equatable/equatable.dart';
import 'package:shopsy/model/Product.dart';

class Wishlist extends Equatable {
  final List<Product> prods;

  const Wishlist({this.prods = const <Product>[]});

  @override
  // TODO: implement props
  List<Object?> get props => [prods];
}
