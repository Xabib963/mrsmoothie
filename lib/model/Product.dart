import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
part "Product.g.dart";

// we create type adapter because product is not primitive type so we cant use it directily
//we want to make uniq id to fields wich will be in type adapter
// after that we run  flutter packages pub run build_runner build

@HiveType(typeId: 0)
class Product extends HiveObject {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String name;
  @HiveField(2)
  final String imgUrl;
  @HiveField(3)
  final double price;
  @HiveField(4)
  final String category;
  @HiveField(5)
  final String description;
  @HiveField(6)
  final double quantity;
  @HiveField(7)
  final bool isrecommended;
  @HiveField(8)
  final bool istrendy;
  Product({
    required this.category,
    required this.description,
    required this.quantity,
    required this.isrecommended,
    required this.istrendy,
    required this.id,
    required this.name,
    required this.imgUrl,
    required this.price,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        name,
        imgUrl,
        quantity,
        isrecommended,
        istrendy,
        price,
        id,
        category,
        description
      ];
  // static List<Product> prods = [
  //   Product(
  //       category: '',
  //       description: '',
  //       isrecommended: true,
  //       istrendy: false,
  //       quantity: 5,
  //       price: 10,
  //       name: 'Water',
  //       id: "a",
  //       imgUrl:
  //           'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80'),
  // ];
  factory Product.fromSnap(QueryDocumentSnapshot doc) {
    return Product(
        category: doc['category'] as String,
        quantity: doc['quantity'] as double,
        isrecommended: doc['isrecommended'] as bool,
        istrendy: doc["istrendy"] as bool,
        id: doc['id'] as String,
        name: doc['name'] as String,
        imgUrl: doc['imgUrl'] as String,
        price: doc['price'] as double,
        description: doc['description'] as String);
  }
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'quantity': quantity,
      'price': price,
      'imgUrl': imgUrl,
      "category": category,
      'isrecommended': isrecommended,
      'istrendy': istrendy,
      'description': description
    };
  }
}
