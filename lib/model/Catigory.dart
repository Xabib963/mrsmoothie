import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class Catigory extends Equatable {
  final String name;
  final String imgUrl;
  final String id;
  Catigory({required this.name, required this.imgUrl, required this.id});
  factory Catigory.fromSnap(QueryDocumentSnapshot doc) {
    return Catigory(
      name: doc['name'] as String,
      imgUrl: doc['imgUrl'],
      id: doc['id'],
    );
  }
  @override
  // TODO: implement props
  List<Object?> get props => [id, name, imgUrl];
  // static List<Catigory> cate = [
  //   Catigory(
  //       name: 'Water', imgUrl: 'assets/pexels-pixabay-327090.jpg', id: 'a'),
  //   Catigory(
  //       name: 'Soft Drinks',
  //       imgUrl: 'assets/pexels-alleksana-4113682.jpg',
  //       id: 'a'),
  //   Catigory(
  //       name: 'Smoothies', imgUrl: 'assets/pexels-pixabay-434295.jpg', id: 'a')
  // ];
}
